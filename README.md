# Blog by Marlon Mena

# Contribuir

:+1: :tada: Primero que nada gracias por tomarte el tiempo para contribuir a este proyecto :+1: :tada:


 ```
 $ git checkout -b devTuNombre
 
 // Despues de hacer tu contribución
 $ git pull origin master
 // Corrige errores, si los hay
 $ git push origin devTuNombre
 ```

* Clona el proyecto
* Crea tu rama de trabajo
* Haz tu contribución
* Baja los últimos cambios en la rama `main`, arregla conflictos si los hay
* Sube tu contribución
* Y haz un pull request a la rama `main` del proyecto.

## Dependencias

* `composer` versión **2**
* `node` versión **18.8.0**
* `Sweet alert 2` versión **11.10.1**
* `Vuejs 3` versión **3.2.36**
* `Vue Router` versión **4.2.5**
* `Front Template` versión **4.3.1**

Haz click en [Front](https://htmlstream.com/preview/front-v4.3.1/documentation/index.html) para saber más acerca de este template.


## :baby_bottle: Baby Steps I

* Copiar y renombrar el archivo `/.env.example` a `.env`.
* Ejecutar en la linea de comandos `$ composer install`
* Ejecutar en la linea de comandos `$ npm install`.
* Ejecutar en la linea de comandos `$ npm run dev`

### Archivo .env

Aquí se encuentra contenidas algunas variables y configuraciones de vital importancia para que el proyecto funcione.

La variable `APP_KEY` se rellena automáticamente ejecutando el comando:

```
$ php artisan key:generate
```

## :baby_bottle: Baby Steps II

### Configuración y creacion de base de datos.

Puedes crear la base de datos ejecutando el siguiente script:

```sql
DROP DATABASE IF EXISTS posts;
CREATE DATABASE posts
   CHARACTER SET = 'utf8mb4'
   COLLATE = 'utf8mb4_unicode_ci';
```

*Nota: una vez creada la base de datos, configura tu conexión*

### Configuración de la conexión

```
DB_CONNECTION=mysql
DB_HOST=YourDatabaseServer
DB_PORT=YourPort
DB_DATABASE=post
DB_USERNAME=YourUsername
DB_PASSWORD=YourPassword
```

### Migraciones con laravel

Ejecutar los siguientes comandos para aplicar las migraciones e inicializar la base datos.

```
$ php artisan migrate && php artisan db:seed --class=DatabaseSeeder

```

## :baby_bottle: Baby Steps III

### Archivos estáticos

Los archivos estáticos como `js` y `css` se guardan en `resources/assets` colocando cada archivo en su carpeta correspondiente según el tipo de archivo. Para mover los archivos
a la carpeta `public` debes configurar el archivo `webpack.mix.js` y ejecutar en la consola el siguiente comando:

``` 
$ npm run dev
$ php artisan optimize
```

Haz click en [Laravel Mix](https://laravel.com/docs/5.5/mix) para saber más acerca de esta configuración.

## :baby_bottle: Baby Steps IV
###  inicia un servidor de desarrollo integrado de Laravel
``` 
$ php artisan serve
```
