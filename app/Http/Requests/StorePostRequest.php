<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required'],
            'author' => ['required'],
            'publication_date' => ['required', 'date'],
            'content' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'title.required' => "El campo título es requerido",
            'author.required' => "El campo autor es requerido",
            'publication_date.required' => "El campo fecha de publicación es requerido",
            'content.required' => "El campo contenido es requerido",
        ];
    }
}
