require('./bootstrap');
require('../assets/js/front');

import { createApp } from 'vue';
import Index from './components/Index.vue';
window.swal = require('sweetalert2');
import router from './components/router/index';

const app = createApp({});
app.use(router);
app.component('example-component', Index);

app.mount('#app');
