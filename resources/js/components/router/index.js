// router/index.js

import { createRouter, createWebHistory } from 'vue-router';
import Detail from '../Show.vue';

const routes = [
    {
        path: '/',
        // ... Otras configuraciones para la vista principal
    },
    {
        path: '/:id',
        name: 'Detail',
        component: Detail,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
