@extends('layouts.public')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="d-md-flex min-vh-md-100 bg-img-hero"
             style="background-image: url(/front/svg/components/shape-5.svg); background-repeat: repeat; background-size: auto">
            <div class="container d-md-flex justify-content-md-center flex-grow-1 content-space-4 min-vh-md-100 space-3 space-md-0">
                <div class="col-sm-12 col-md-8 col-lg-7 col-xl-5">
                    <!-- End Blog Card -->
                    <div class="card shadow-lg">
                        <div class="card-header">
                            <h4 class="card-header-title">Iniciar sesión</h4>
                        </div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <p class="text-justify text-muted">Ingrese su usuario y contraseña.
                                </p>
                                <div class="form-group row mb-4">
                                    <label for="email"
                                           class="col-md-4 form-label text-md-right">{{ __('Usuario') }}</label>

                                    <div class="col-md-8">
                                        <input id="email" type="text"
                                               class="form-control form-control-lg @error('email') is-invalid @enderror"
                                               name="email"
                                               value="marlonmanuel342@gmail.com" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label for="password"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                                    <div class="col-md-8">
                                        <input id="password" type="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               value="M4rl0n2!#"
                                               name="password" required autocomplete="current-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row d-none">
                                    <div class="col-md-8 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember"
                                                   id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Recordar mis datos') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-grid mb-3">
                                    <button type="submit" class="btn btn-primary btn-lg transition-3d-hover btn-block"
                                            id="btn_sesion">Iniciar sesión
                                    </button>
                                </div>
                                <div class="container-fluid text-center">

                                </div>
                            </form>

                        </div>
                        <!-- End Blog Card -->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
