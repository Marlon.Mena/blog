<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Blog') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/front/vendor/bootstrap-icons/font/bootstrap-icons.css">
    <!-- CSS  -->
    <link href="{{ asset('front/css/vendor.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/docs.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/snippets.css') }}" rel="stylesheet">
    <link href="{{ asset('css/front.css') }}" rel="stylesheet">

    @yield('extra-css')
</head>
<body>
<!-- ========== HEADER ========== -->
<header class="docs-navbar navbar navbar-expand-lg navbar-end navbar-light bg-white">
    <div class="container">
        <div class="js-mega-menu navbar-nav-wrap">
            <!-- Logo -->
            <a class="navbar-brand p-0" href="/" aria-label="Blog">
                <span class="text-muted align-middle">Blog</span>
            </a>
            <!-- End Logo -->
            <!-- Toggle -->
            <button type="button" class="navbar-toggler ms-auto" data-bs-toggle="collapse" data-bs-target="#navbarNavMenuWithMegaMenu" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navbarNavMenuRightAligned">
                    <span class="navbar-toggler-default">
                      <i class="bi-list"></i>
                    </span>
                <span class="navbar-toggler-toggled">
                      <i class="bi-x"></i>
                    </span>
            </button>
            <nav class="navbar-nav-wrap-col collapse navbar-collapse" id="navbarNavMenuWithMegaMenu">
                <!-- Navbar -->
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarRightAlignedDropdownSubMenu" role="button" data-bs-toggle="dropdown" aria-expanded="false">Menú</a>
                        <div class="dropdown-menu" aria-labelledby="navbarRightAlignedDropdownSubMenu" style="min-width: 230px;">
                            <a class="dropdown-item" href="/"> <i class="bi-house mr-2"></i> Inicio</a>

                            <a class="dropdown-item "
                               href="javascript:;"
                               data-toggle="tooltip" data-placement="right"
                               onclick="event.preventDefault();
									   document.getElementById('logout-form1').submit();">
                                <i class="bi-power mr-2"></i> Cerrar sesión
                            </a>
                        </div>
                    </li>
                </ul>
                <!-- End Navbar -->
                <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                <!-- End Navbar -->
            </nav>
        </div>
    </div>
</header>
<!-- ========== END HEADER ========== -->

<!-- ========== MAIN ========== -->

<main id="content" role="main" class="bg-light pb-8">
    <!-- Content Section -->
    <div class="space-1 space-top-lg-1 mt-lg-1 mx-auto" style="width: 75vw">
        <div class="col-lg-12 p-sm-0 p-md-2" style="padding-right: 2px;padding-left: 2px;">
            <h2 class="text-navy pl-2 mb-0"></h2>
            @yield('content')
        </div>

        <!-- End Row -->
    </div>
    <!-- End Content Section -->
</main>
<!-- ========== END MAIN ========== -->
<footer class="container space-top-2 space-top-md-1" style=" bottom: 0; left: 10%;">
    <div class="d-flex justify-content-between align-items-center py-5">
        <ul class="list-inline mb-0">
            <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" target="_blank">
                    <i class="bi bi-facebook"></i>
                </a>
            </li>

            <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent"  target="_blank">
                    <i class="bi bi-twitter"></i>
                </a>
            </li>
        </ul>
        <!-- End Social Networks -->
    </div>
</footer>

<!-- ========== END SECONDARY CONTENTS ========== -->
<!-- Go to Top -->
<a class="js-go-to go-to position-fixed" href="javascript:;" style="visibility: hidden;"
   data-hs-go-to-options='{
     "offsetTop": 300,
     "position": {
       "init": {
         "right": 15
       },
       "show": {
         "bottom": 15
       },
       "hide": {
         "bottom": -15
       }
     }
   }'>
    <i class="fas fa-arrow-up"></i>
</a>
<!-- End Go to Top -->

<!-- Scripts -->
<!-- JS Global Compulsory -->
<script src="{{ asset('front/js/vendor.min.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.core.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.bs-dropdown.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.bs-validation.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.chartjs.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.circles.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.dropzone.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.imask.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.leaflet.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.list.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.nouislider.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.quill.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.tom-select.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.typed.js') }}"></script>
@yield('scripts')
@yield('extra-scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10" defer></script>

@stack('scripts')
</body>
</html>
