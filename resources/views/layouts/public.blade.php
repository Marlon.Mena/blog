<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>{{config('app.name', 'Laravel')}}</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    @includeWhen((config('app.env') === 'production'),'layouts.partials._analytics')
    <!-- Google Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="/front/vendor/bootstrap-icons/font/bootstrap-icons.css">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSS  -->
    <link href="{{ asset('front/css/vendor.min.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/docs.css') }}" rel="stylesheet">
    <link href="{{ asset('front/css/snippets.css') }}" rel="stylesheet">
    <link href="{{ asset('css/front.css') }}" rel="stylesheet">
    @yield('extra-css')
</head>
<body>
<!-- ========== HEADER ========== -->


<header id="header" class="header header-bg-transparent header-white-nav-links-lg header-abs-top"
        data-hs-header-options="{
            &quot;fixMoment&quot;: 1000,
            &quot;fixEffect&quot;: &quot;slide&quot;
          }">
    <div class="header-section">
        <div id="logoAndNav" class="container">
            <!-- Nav -->
            <nav class="js-mega-menu navbar navbar-expand-lg hs-menu-initialized hs-menu-horizontal">
                <!-- Logo
                <a class="navbar-brand navbar-nav-wrap-brand" href="/" aria-label="Front">
                    <img src="/images/e-gestion-logo.jpg" alt="Logo" style="max-width: 80px">
                </a>-->
                <!-- End Logo -->

                <!-- Responsive Toggle Button -->
                <button type="button" class="navbar-toggler btn btn-icon btn-sm rounded-circle"
                        aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar"
                        data-toggle="collapse" data-target="#navBar">
            <span class="navbar-toggler-default">
              <svg width="14" height="14" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
                <path fill="currentColor"
                      d="M17.4,6.2H0.6C0.3,6.2,0,5.9,0,5.5V4.1c0-0.4,0.3-0.7,0.6-0.7h16.9c0.3,0,0.6,0.3,0.6,0.7v1.4C18,5.9,17.7,6.2,17.4,6.2z M17.4,14.1H0.6c-0.3,0-0.6-0.3-0.6-0.7V12c0-0.4,0.3-0.7,0.6-0.7h16.9c0.3,0,0.6,0.3,0.6,0.7v1.4C18,13.7,17.7,14.1,17.4,14.1z"></path>
              </svg>
            </span>
                    <span class="navbar-toggler-toggled">
              <svg width="14" height="14" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
                <path fill="currentColor"
                      d="M11.5,9.5l5-5c0.2-0.2,0.2-0.6-0.1-0.9l-1-1c-0.3-0.3-0.7-0.3-0.9-0.1l-5,5l-5-5C4.3,2.3,3.9,2.4,3.6,2.6l-1,1 C2.4,3.9,2.3,4.3,2.5,4.5l5,5l-5,5c-0.2,0.2-0.2,0.6,0.1,0.9l1,1c0.3,0.3,0.7,0.3,0.9,0.1l5-5l5,5c0.2,0.2,0.6,0.2,0.9-0.1l1-1 c0.3-0.3,0.3-0.7,0.1-0.9L11.5,9.5z"></path>
              </svg>
            </span>
                </button>
                <!-- End Responsive Toggle Button -->

                <!-- Navigation -->
                <div id="navBar" class="collapse navbar-collapse">

                    @yield('header_nav_items')

                </div>
                <!-- End Navigation -->
            </nav>
            <!-- End Nav -->
        </div>
    </div>
</header>

<main id="content" role="main">
    @yield('content')

    <!-- Go to Top -->
    <a class="js-go-to go-to position-fixed" href="javascript:;" style="visibility: hidden;"
       data-hs-go-to-options='{
     "offsetTop": 300,
     "position": {
       "init": {
         "right": 15
       },
       "show": {
         "bottom": 15
       },
       "hide": {
         "bottom": -15
       }
     }
   }'>
        <i class="fas fa-arrow-up"></i>
    </a>
    <!-- Go to Top -->
</main>
<footer class="container space-1">
    <div class="d-flex justify-content-between align-items-center py-5">
        <!-- Copyright -->
        <!-- End Copyright -->
        <!-- Social Networks -->
        <ul class="list-inline mb-0">
            <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent"
                     target="_blank">
                    <i class="bi bi-facebook"></i>
                </a>
            </li>

            <li class="list-inline-item">
                <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent"
                   target="_blank">
                    <i class="bi bi-twitter"></i>
                </a>
            </li>
        </ul>
        <!-- End Social Networks -->
    </div>
</footer>

<script src="{{ asset('front/js/vendor.min.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.core.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.bs-dropdown.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.bs-validation.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.chartjs.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.circles.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.dropzone.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.imask.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.leaflet.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.list.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.nouislider.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.quill.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.tom-select.js') }}"></script>
<script src="{{ asset('front/assets/js/hs.typed.js') }}"></script>

<script src="{{ asset('js/public.js') }}"></script>
@yield('scripts')
</body>
</html>


