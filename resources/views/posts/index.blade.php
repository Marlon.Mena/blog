@extends('layouts.app')

@section('css-extra')
    <style>
    </style>
@endsection
@section('content')
    <div id="app">
        <example-component></example-component>
    </div>
@stop
@push('scripts')
    <script src="{{ asset('front/js/vendor.min.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush
