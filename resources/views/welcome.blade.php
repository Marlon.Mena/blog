@extends('layouts.public')

@section('extra-css')
@endsection

@section('header_nav_items')
@stop

@section('content')


    <!-- Hero -->
    <div class="overflow-hidden">
        <div class="container text-center content-space-b-3 justify-content-center">
            <div class="row justify-content-lg-between align-items-md-center">
                <div class="col-md-12">
                    <div class="position-relative">
                        <div class="bg-light" id="loginSection">

                            <!-- Blog Listing Section -->

                            <div class="d-md-flex min-vh-md-100 bg-img-hero"
                                 style="background-image: url(/front/svg/components/shape-5.svg); background-repeat: repeat; background-size: auto">
                                <div class="container d-md-flex justify-content-md-center flex-grow-1 content-space-4 min-vh-md-100 space-3 space-md-0">
                                    <div class="col-sm-12 col-md-8 col-lg-7 col-xl-5">
                                        <!-- End Blog Card -->
                                        <div class="card shadow-lg">
                                            <div class="card-header">
                                                <h4 class="card-header-title">Iniciar sesión</h4>
                                            </div>

                                            <div class="card-body">
                                                <form method="POST" action="{{ route('login') }}">
                                                    @csrf

                                                    <p class="text-justify text-muted">Ingrese su usuario y contraseña.
                                                    </p>
                                                    <div class="form-group row mb-4">
                                                        <label for="email"
                                                               class="col-md-4 form-label text-md-right">{{ __('Usuario') }}</label>

                                                        <div class="col-md-8">
                                                            <input id="email" type="text"
                                                                   class="form-control form-control-lg @error('email') is-invalid @enderror"
                                                                   name="email"
                                                                   value="marlonmanuel342@gmail.com" required autocomplete="email">

                                                            @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="form-group row mb-4">
                                                        <label for="password"
                                                               class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                                                        <div class="col-md-8">
                                                            <input id="password" type="password"
                                                                   class="form-control @error('password') is-invalid @enderror"
                                                                   value="M4rl0n2!#"
                                                                   name="password" required autocomplete="current-password">

                                                            @error('password')
                                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="form-group row d-none">
                                                        <div class="col-md-8 offset-md-4">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox" name="remember"
                                                                       id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                                <label class="form-check-label" for="remember">
                                                                    {{ __('Recordar mis datos') }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="d-grid mb-3">
                                                        <button type="submit" class="btn btn-primary btn-lg transition-3d-hover btn-block"
                                                                id="btn_sesion">Iniciar sesión
                                                        </button>
                                                    </div>
                                                    <div class="container-fluid text-center">

                                                    </div>
                                                </form>

                                            </div>
                                            <!-- End Blog Card -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Blog Listing Section -->
                        </div>
                    </div>
                </div>
                <!-- End Col -->
            </div>
            <!-- End Row -->
        </div>
    </div>
    <figure>
        <svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg"
             x="0px" y="0px" width="100%" height="64px" viewBox="0 0 1921 273"
             style="margin-bottom: -8px; enable-background:new 0 0 1921 273;" xml:space="preserve">
        <polygon fill="#f9fbff" points="0,273 1921,273 1921,0 "></polygon>
      </svg>
    </figure>
@stop

@section('scripts')
    <!-- JS Plugins Init. -->
    @if(old('username') )
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $('html, body').animate({
                        scrollTop: parseInt($("#loginSection").offset().top)
                    }, 100);
                }, 100);
            });
        </script>
    @endif
@stop
