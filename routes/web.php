<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web router for your application. These
| router are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Auth::routes();
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/', [App\Http\Controllers\PostController::class,'index'])->name('home');
});

Route::view('/welcome', 'welcome')->name('welcome')->middleware('guest');
