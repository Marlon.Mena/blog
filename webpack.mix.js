const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.copyDirectory('resources/assets/front', 'public/front');
mix.copyDirectory('resources/assets/file-input', 'public/file-input');
mix.sass('resources/assets/sass/front.scss', 'public/css');

mix.js('resources/assets/js/front.js', 'public/js');
mix.js('resources/assets/js/public.js', 'public/js');


mix.js('resources/js/app.js', 'public/js').vue()
mix.sourceMaps();

